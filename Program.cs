﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using WinSCP;
using FtpLib;
using System.Collections.Generic;

namespace FTP_Communicator
{
    class Program
    {

        static void Main(string[] args)
        {
            var FTPlog = "dlpuser@dlptest.com";
            var FTPpass = "SzMf7rTE4pCrf9dV286GuNe4N";
            var FTPadd = "ftp.dlptest.com";
            var localDirectory = @"D:\Stage";
            var Test = @"D:\Stage\TESTFTP123.txt";

            //connection au serveur FTP
            FtpConnection connection = new FtpConnection(FTPadd, FTPlog, FTPpass);
            //je stock les infos de connexion dans une variable
            var Connection = connection;

            //Download files
            using (Connection)
            {
                //on se connecte au FTP
                Connection.Open();
                Connection.Login();

                //on selectionne le directory qu'on veut ciblé
                Connection.SetCurrentDirectory("/");
                //on stock dans une variable les noms des fichiers du rep
                var files = Connection.GetFiles();

                //pour chaque fichier present dans le dossier, on le copie dans le fichier ciblé local et on les supprimes
                foreach (var file in files)
                {
                    Connection.GetFile(file.FullName, localDirectory + @"\" + file.Name, false);
                    Connection.RemoveFile(file.Name);
                }
                //on ferme
                Connection.Close();
            }

            //upload files
            using(Connection)
            {
                //on se connect au FTP
                Connection.Open();
                Connection.Login();

                //on cible le dossier voulu 
                Connection.SetCurrentDirectory("/");
                //on insère le fichier que l'on veut upload dans le nouveau rep
                Connection.PutFile(Test);
                //et on ferme
                Connection.Close();
            }

        }
    }
}
